tf-aws-rds
==========

Create RDS instances with support for read replicas.

## Terraform version compatibility

| Module version | Terraform version |
|----------------|-------------------|
| 2.x.x          | 0.12.x            |
| <= 1.x.x       | 0.11.x            |

Usage
-----

```js

Basic Usage:

module "rds" {
  source                      = "../modules/tf-aws-rds"
  name                        = "${var.envname}-rds"
  subnets                     = "${module.vpc.private_subnets}"
  security_groups             = ["${aws_security_group.db-ingress.id}"]
  engine                      = "mysql"
  final_snapshot_identifier   = "final-${var.envname}-snapshot"

  tags = concat(
    [  
    { 
      key = "customer" 
      value = "${var.account}"
      },
    { 
      key = "envtype" 
      value = "${var.envtype}"
      },
    { 
      key = "envname" 
      value = "${var.envname}"
      }
  ],
    var.signagerds_tags
  )
}


Advanced Usage:

module "rds" {
  source                      = "git::ssh://git@gogs.bashton.net/Bashton-Terraform-Modules/tf-aws-rds.git"
  name                        = "${var.envname}-rds"
  subnets                     = "${module.vpc.private_subnets}"
  security_groups             = ["${aws_security_group.db-ingress.id}"]
  engine_version              = "${lookup(var.rds, "engine_version")}"
  snapshot_identifier         = "${lookup(var.rds, "snapshot_identifier", "")}"
  password                    = "${lookup(var.rds, "rootpw")}"
  instance_type               = "${lookup(var.rds, "instance_type")}"
  storage_size                = "${lookup(var.rds, "storage_size")}"
  storage_type                = "${lookup(var.rds, "storage_type")}"
  storage_encrypted           = "${lookup(var.rds, "storage_encrypted")}"
  iops                        = "${lookup(var.rds, "iops", "0")}"
  multi_az                    = "${lookup(var.rds, "multi_az")}"
  backup_retention_period     = "${lookup(var.rds, "backup_retention_period")}"
  monitoring_interval         = "${lookup(var.rds, "monitoring_interval"}"
  parameter_group             = "${aws_db_parameter_group.rds_pg.id}"
  read_replica_count          = "${lookup(var.rds, "read_replica_count")}"
  read_replica_instance_class = "${lookup(var.rds, "read_replica_instance_class")}"
  apply_immediately           = "${lookup(var.rds, "apply_immediately")}"
  skip_final_snapshot         = "${var.skip_final_snapshot}"
  final_snapshot_identifier   = "final-${var.envname}-snapshot"
  cw_sns_topic                = ["${aws_sns_topic.sns_pagerduty.id}"]

  tags = concat(
    [  
    { 
      key = "customer" 
      value = "${var.account}"
      },
    { 
      key = "envtype" 
      value = "${var.envtype}"
      },
    { 
      key = "envname" 
      value = "${var.envname}"
      }
  ],
    var.signagerds_tags
  )
}

#Sample Variable Map for tfvars

rds = {
  snapshot_identifier         = ""
  instance_type               = "db.m3.xlarge"
  multi_az                    = false
  backup_retention_period     = "1"
  storage_size                = "200"
  storage_type                = "gp2"
  rootpw                      = "RainbowEnvironments!"
  read_replica_count          = "2"
  read_replica_instance_class = "db.m3.large"
  engine  	                  = "mysql"
  engine_version              = "5.6.34"
}



```

Variables
---------

 - `subnets` - list of subnets to deploy into
 - `security_groups` - list of security groups to deploy into
 - `engine` - RDS engine to use
 - `engine_version`- RDS engine version to use
 - `character_set_name` - character set name to use for DB encoding in Oracle
 - `snapshot_identifier` - snapshot identifier
 - `password` - default root password
 - `instance_type` - instance type to use
 - `storage_size` - storage volume size (Gb)
 - `storage_type` - EBS type
 - `storage_encrypted` - true or false
 - `iops` - IOPS to provision if io EBS
 - `multi_az` - true or false
 - `backup_retention_period` - days retention for snapshots
 - `monitoring_interval` - interval between points when enhanced monitoring metrics is enabled
 - `parameter_group` - paramater group to use if non default for engine
 - `read_replica_count` - number of read replicas
 - `read_replica_instance_class` - instance type of read replicas
 - `apply_immediately` - true or false
 - `skip_final_snapshot` - true or false
 - `final_snapshot_identifier` - name of final snapshot identifier
 - `cw_sns_topic` - SNS id. Setting enables CloudWatch alarms for OK and RECOVERY states
 - `allow_major_version_upgrade` - Determines whether major version upgrades are allowed by the RDS instance(s)
 - `db_subnet_group_name` - Option to specify the DB subnet group name
 - `ca_cert_identifier` - Specifies the identifier of the CA certificate for the DB instance (Valid values: `rds-ca-2015` and `rds-ca-2019`)
 - `domain_iam_role_name` - The name of the IAM role to be used when making API calls to the Directory Service
 - `domain` - The ID of the Directory Service Active Directory domain the instance is joined to
 - `kms_key_id` - The ARN of a KMS key to use for encryption
 - `tags` - Optional list of tags to add to resources

Outputs
-------

 - `vpc_id` - the VPC id
 - `vpc_cidr` - assigned CIDR block of the VPC
 - `availability_zones` - comma separated list of availability zones
 - `public_subnets` - comma separated list of public subnet ids
 - `public_route_tables` - comma separated list of public route table ids
 - `private_subnets` - comma separated list of private subnet ids
 - `private_route_tables` - comma separated list of private route table ids
 - `endpoint` - the connection endpoint
 - `address` - the address of the RDS instance
 - `arn` - ARN of the RDS instance
 - `identifier` - the RDS instance ID
 - `read_replica_addresses` - the address of the RDS instance read replias
