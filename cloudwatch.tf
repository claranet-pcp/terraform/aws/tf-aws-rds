locals {
  # Find the default max connections for this instance type.
  instance_type_max_conns = lookup(var.cw_rds_instance_type_max_conns, var.instance_type, var.cw_rds_max_conns)

  # Try to find a more specific limit for this instance type + engine or fall back to the default.
  instance_max_conns = lookup(var.cw_rds_instance_type_max_conns, "${var.instance_type}.${var.engine}", local.instance_type_max_conns)

  # Calculate the max connections threshold as a percentage of the above value.
  max_conns_percentage = floor(local.instance_type_max_conns * coalesce(var.cw_rds_max_conns_percent, 0) / 100)

  # If a specific number has been passed in for the max connections alarm, use that. Otherwise use the calculated value.
  max_conns = coalesce(var.cw_rds_max_conns, local.max_conns_percentage)
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_DatabaseConnections" {
  count = var.enabled && local.max_conns != 0 ? 1 : 0

  alarm_name          = "${var.name}_alarm_rds_DatabaseConnections"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "DatabaseConnections"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Maximum"
  threshold           = local.max_conns
  alarm_description   = "RDS Maximum connection Alarm for ${join("", aws_db_instance.default.*.identifier)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions =  {
    DBInstanceIdentifier = join("", aws_db_instance.default.*.identifier)
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_DatabaseConnections-rr" {
  count = var.enabled && local.max_conns != 0 ? var.read_replica_count : 0

  alarm_name          = "${var.name}_${element(aws_db_instance.read_replica.*.identifier, count.index)}_alarm_rds_DatabaseConnections"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "DatabaseConnections"
  namespace           = "AWS/RDS"
  period              = "60"
  statistic           = "Maximum"
  threshold           = local.max_conns
  alarm_description   = "RDS Maximum connection Alarm for ${element(aws_db_instance.read_replica.*.identifier, count.index)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = aws_db_instance.read_replica.*.identifier[count.index]
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_CPU" {
  count = var.enabled ? 1 : 0

  alarm_name          = "${var.name}_alarm_rds_CPU"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = var.cw_rds_cpu_occurance
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = var.cw_rds_cpu_periods
  statistic           = "Average"
  threshold           = var.cw_rds_cpu_threshold
  alarm_description   = "RDS CPU Alarm for ${join("", aws_db_instance.default.*.identifier)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = join("", aws_db_instance.default.*.identifier)
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_CPU-rr" {
  count = var.enabled ? var.read_replica_count : 0

  alarm_name          = "${var.name}_${element(aws_db_instance.read_replica.*.identifier, count.index)}_alarm_rds_CPU"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = var.cw_rds_cpu_occurance
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = var.cw_rds_cpu_periods
  statistic           = "Average"
  threshold           = var.cw_rds_cpu_threshold
  alarm_description   = "RDS CPU Alarm for ${element(aws_db_instance.read_replica.*.identifier, count.index)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = aws_db_instance.read_replica.*.identifier[count.index]
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_diskspace" {
  count = var.enabled ? 1 : 0

  alarm_name          = "${var.name}_alarm_rds_diskspace"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "FreeStorageSpace"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = var.storage_size * 10000000 * var.cw_rds_disk_threshold
  alarm_description   = "RDS diskspace Alarm for ${join("", aws_db_instance.default.*.identifier)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = join("", aws_db_instance.default.*.identifier)
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_diskspace-rr" {
  count = var.enabled ? var.read_replica_count : 0

  alarm_name          = "${var.name}_${element(aws_db_instance.read_replica.*.identifier, count.index)}_alarm_rds_diskspace"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "FreeStorageSpace"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = var.storage_size * 10000000 * var.cw_rds_disk_threshold
  alarm_description   = "RDS diskspace Alarm for ${element(aws_db_instance.read_replica.*.identifier, count.index)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = aws_db_instance.read_replica.*.identifier[count.index]
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_DiskQueueDepth" {
  count = var.enabled && var.cw_rds_disk_queue_depth_threshold != "" ? 1 : 0

  alarm_name          = "${var.name}_alarm_rds_DiskQueueDepth"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "DiskQueueDepth"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = var.cw_rds_disk_queue_depth_threshold
  alarm_description   = "RDS DiskQueueDepth Alarm for ${join("", aws_db_instance.default.*.identifier)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = join("", aws_db_instance.default.*.identifier)
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_DiskQueueDepth-rr" {
  count = var.enabled && var.cw_rds_disk_queue_depth_threshold != "" ? var.read_replica_count : 0

  alarm_name          = "${var.name}_${element(aws_db_instance.read_replica.*.identifier, count.index)}_alarm_rds_DiskQueueDepth"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "DiskQueueDepth"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = var.cw_rds_disk_queue_depth_threshold
  alarm_description   = "RDS DiskQueueDepth Alarm for ${element(aws_db_instance.read_replica.*.identifier, count.index)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = aws_db_instance.read_replica.*.identifier[count.index]
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_ReadLatency" {
  count = var.enabled && var.cw_rds_read_latency_threshold != "" ? 1 : 0

  alarm_name          = "${var.name}_alarm_rds_ReadLatency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "ReadLatency"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = var.cw_rds_read_latency_threshold
  alarm_description   = "RDS ReadLatency Alarm for ${join("", aws_db_instance.default.*.identifier)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = join("", aws_db_instance.default.*.identifier)
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_ReadLatency-rr" {
  count = var.enabled && var.cw_rds_read_latency_threshold != "" ? var.read_replica_count : 0

  alarm_name          = "${var.name}_${element(aws_db_instance.read_replica.*.identifier, count.index)}_alarm_rds_ReadLatency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "ReadLatency"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = var.cw_rds_read_latency_threshold
  alarm_description   = "RDS ReadLatency Alarm for ${element(aws_db_instance.read_replica.*.identifier, count.index)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = aws_db_instance.read_replica.*.identifier[count.index]
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_WriteLatency" {
  count = var.enabled && var.cw_rds_write_latency_threshold != "" ? 1 : 0

  alarm_name          = "${var.name}_alarm_rds_WriteLatency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "WriteLatency"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = var.cw_rds_write_latency_threshold
  alarm_description   = "RDS WriteLatency Alarm for ${join("", aws_db_instance.default.*.identifier)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = join("", aws_db_instance.default.*.identifier)
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_WriteLatency-rr" {
  count = var.enabled && var.cw_rds_write_latency_threshold != "" ? var.read_replica_count : 0

  alarm_name          = "${var.name}_${element(aws_db_instance.read_replica.*.identifier, count.index)}_alarm_rds_WriteLatency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "WriteLatency"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = var.cw_rds_write_latency_threshold
  alarm_description   = "RDS WriteLatency Alarm for ${element(aws_db_instance.read_replica.*.identifier, count.index)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = aws_db_instance.read_replica.*.identifier[count.index]
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_SwapUsage" {
  count = var.enabled && var.cw_rds_swap_threshold != "" ? 1 : 0

  alarm_name          = "${var.name}_alarm_rds_SwapUsage"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "3"
  metric_name         = "SwapUsage"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Maximum"
  threshold           = var.cw_rds_swap_threshold
  alarm_description   = "RDS SwapUsage Alarm for ${join("", aws_db_instance.default.*.identifier)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = join("", aws_db_instance.default.*.identifier)
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_SwapUsage-rr" {
  count = var.enabled && var.cw_rds_swap_threshold != "" ? var.read_replica_count : 0

  alarm_name          = "${var.name}_${element(aws_db_instance.read_replica.*.identifier, count.index)}_alarm_rds_SwapUsage"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "3"
  metric_name         = "SwapUsage"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Maximum"
  threshold           = var.cw_rds_swap_threshold
  alarm_description   = "RDS SwapUsage Alarm for ${element(aws_db_instance.read_replica.*.identifier, count.index)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = aws_db_instance.read_replica.*.identifier[count.index]
  }
}

resource "aws_cloudwatch_metric_alarm" "alarm_rds_replica_lag-rr" {
  count = var.enabled ? var.read_replica_count : 0

  alarm_name          = "${var.name}_${element(aws_db_instance.read_replica.*.identifier, count.index)}_alarm_rds_replicalag"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "ReplicaLag"
  namespace           = "AWS/RDS"
  period              = "60"
  statistic           = "Average"
  threshold           = var.cw_rds_replica_lag
  alarm_description   = "RDS CPU Alarm for ${element(aws_db_instance.read_replica.*.identifier, count.index)}"
  alarm_actions       = compact(var.cw_sns_topic)
  ok_actions          = compact(var.cw_sns_topic)
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = aws_db_instance.read_replica.*.identifier[count.index]
  }
}
