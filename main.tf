resource "aws_db_subnet_group" "main" {
  count = var.enabled ? 1 : 0

  name        = coalesce(var.db_subnet_group_name, "${var.name}-subnet")
  description = "Private subnets"
  subnet_ids  = var.subnets

  tags = length(var.tags) > 0 ? var.tags : {
    customer = var.customer
    envname  = var.envname
    envtype  = var.envtype
  }
}

resource "aws_db_instance" "default" {
  count                           = var.enabled ? 1 : 0
  identifier                      = var.name
  snapshot_identifier             = var.snapshot_identifier
  allocated_storage               = var.storage_size
  storage_type                    = var.iops == 0 ? var.storage_type : "io1"
  storage_encrypted               = var.storage_encrypted
  iops                            = var.iops
  engine                          = var.engine
  engine_version                  = var.engine_version
  character_set_name              = var.character_set_name
  instance_class                  = var.instance_type
  license_model                   = var.license_model
  username                        = var.username
  password                        = var.password
  multi_az                        = var.multi_az
  parameter_group_name            = var.parameter_group
  option_group_name               = var.option_group
  db_subnet_group_name            = aws_db_subnet_group.main[0].name
  vpc_security_group_ids          = var.security_groups
  backup_retention_period         = var.backup_retention_period
  backup_window                   = var.backup_window
  maintenance_window              = var.maintenance_window
  monitoring_interval             = var.monitoring_interval
  auto_minor_version_upgrade      = var.auto_minor_version_upgrade
  monitoring_role_arn             = join("", aws_iam_role.monitoring_role.*.arn)
  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports
  apply_immediately               = var.apply_immediately
  skip_final_snapshot             = var.skip_final_snapshot
  final_snapshot_identifier       = var.final_snapshot_identifier
  allow_major_version_upgrade     = var.allow_major_version_upgrade
  ca_cert_identifier              = var.ca_cert_identifier
  domain_iam_role_name            = var.domain_iam_role_name
  domain                          = var.domain
  kms_key_id                      = var.kms_key_id
  performance_insights_enabled    = var.performance_insights_enabled
  performance_insights_kms_key_id = var.performance_insights_kms_key_id

  tags = length(var.tags) > 0 ? var.tags : {
    customer = var.customer
    envname  = var.envname
    envtype  = var.envtype
  }
}

resource "aws_db_instance" "read_replica" {
  count                       = var.enabled ? var.read_replica_count : 0
  replicate_source_db         = join("", aws_db_instance.default.*.id)
  instance_class              = var.read_replica_instance_class
  identifier                  = "${var.name}-rr${count.index + 1}"
  storage_type                = var.iops == 0 ? var.storage_type : "io1"
  iops                        = var.iops
  multi_az                    = var.read_replica_multi_az
  skip_final_snapshot         = var.read_replica_skip_final_snapshot
  parameter_group_name        = var.parameter_group
  option_group_name           = var.option_group
  vpc_security_group_ids      = var.security_groups
  backup_window               = var.backup_window
  maintenance_window          = var.maintenance_window
  apply_immediately           = var.apply_immediately
  allocated_storage           = var.storage_size
  allow_major_version_upgrade = var.allow_major_version_upgrade
  kms_key_id                  = var.kms_key_id

  tags = length(var.tags) > 0 ? var.tags : {
    customer = var.customer
    envname  = var.envname
    envtype  = var.envtype
  }
}