/* Environment variables */
variable "name" {
  description = "This value will prefix the name on all resources created by this module"
  type        = string
}

variable "customer" {
  description = "This value will be added as the 'customer' tags on resources created by this module"
  type        = string
}

variable "envname" {
  description = "This value will be added as the 'envname' tags on resources created by this module"
  type        = string
}

variable "envtype" {
  description = "This value will be added as the 'envtype' tags on resources created by this module"
  type        = string
}

/* Instance variables */
variable "domain" {
  description = "The ID of the Directory Service Active Directory domain the instance is joined to."
  type        = string
  default     = null
}

variable "domain_iam_role_name" {
  description = "The name of the IAM role to be used when making API calls to the Directory Service."
  type        = string
  default     = null
}

variable "instance_type" {
  description = "The instance type for the master RDS instance"
  type        = string
  default     = "db.t2.micro"
}

variable "monitoring_interval" {
  description = "The interval, in seconds, between points when Enhanced Monitoring metrics are collected. To disable collecting Enhanced Monitoring metrics, specify 0. Valid Values: 0, 1, 5, 10, 15, 30, 60."
  type        = string
  default     = "0"
}

variable "read_replica_instance_class" {
  description = "The instance type for the RDS read replica(s)"
  type        = string
  default     = "db.t2.micro"
}

variable "subnets" {
  description = "A list of VPC subnet IDs that will be designated as DB subnets"
  type        = list(string)
}

variable "read_replica_count" {
  description = "The count of read replicas you would like to create"
  type        = string
  default     = 0
}

variable "read_replica_multi_az" {
  description = "Specifies if the RDS read replica(s) are multi-AZ"
  type        = string
  default     = false
}

variable "storage_size" {
  description = "The allocated storage for RDS instances (GB)"
  type        = string
  default     = "5"
}

variable "storage_type" {
  description = "The allocated storage type for RDS instances"
  type        = string
  default     = "gp2"
}

variable "storage_encrypted" {
  description = "Bool indicating whether the DB instance is encrypted"
  type        = string
  default     = false
}

variable "iops" {
  description = "The amount of provisioned IOPS. Setting this implies a storage_type of 'io1'"
  type        = number
  default     = 0
}

variable "username" {
  description = "Username for the master DB user"
  type        = string
  default     = "root"
}

variable "password" {
  description = "Password for the master DB user"
  type        = string
  default     = "changeme"
}

variable "multi_az" {
  description = "Bool indicating whether the RDS instance is multi AZ enabled"
  type        = string
  default     = false
}

/* CloudWatch variables */
variable "cw_sns_topic" {
  description = "SNS topic to alert on CloudWatch alarms"
  type        = list(string)
  default     = [""]
}

variable "cw_rds_max_conns" {
  description = "Maximum RDS connections to trigger alert"
  type        = string
  default     = ""
}

variable "cw_rds_max_conns_percent" {
  description = "Maximum RDS connections (calculated percentage) to trigger alert"
  type        = string
  default     = "80"
}

variable "cw_rds_instance_type_max_conns" {
  description = "Maximum RDS connections per instance type"
  type        = map(string)

  default = {
    "db.t2.micro"            = "66"
    "db.t2.micro.postgres"   = "104"
    "db.t2.small"            = "150"
    "db.t2.small.aurora"     = "45"
    "db.t2.small.postgres"   = "209"
    "db.t2.medium"           = "312"
    "db.t2.medium.aurora"    = "90"
    "db.t2.medium.postgres"  = "419"
    "db.t2.large"            = "648"
    "db.t2.large.postgres"   = "839"
    "db.t2.xlarge"           = "1365"
    "db.t2.xlarge.postgres"  = "1678"
    "db.t2.2xlarge"          = "2730"
    "db.t2.2xlarge.postgres" = "3357"

    "db.m3.medium"           = "296"
    "db.m3.medium.postgres"  = "393"
    "db.m3.large"            = "609"
    "db.m3.large.postgres"   = "786"
    "db.m3.xlarge"           = "1237"
    "db.m3.xlarge.postgres"  = "1573"
    "db.m3.2xlarge"          = "2492"
    "db.m3.2xlarge.postgres" = "3147"

    "db.m4.large"           = "648"
    "db.m4.large.postgres"  = "839"
    "db.m4.xlarge"          = "1320"
    "db.m4.xlarge.postgres" = "1678"

    "db.m5.large"           = "648"

    "db.r3.large"            = "1258"
    "db.r3.large.aurora"     = "1000"
    "db.r3.large.postgres"   = "1600"
    "db.r3.xlarge"           = "2540"
    "db.r3.xlarge.aurora"    = "2000"
    "db.r3.xlarge.postgres"  = "3200"
    "db.r3.2xlarge"          = "5080"
    "db.r3.2xlarge.aurora"   = "3000"
    "db.r3.2xlarge.postgres" = "5000"
    "db.r3.4xlarge"          = "10160"
    "db.r3.4xlarge.aurora"   = "4000"
    "db.r3.4xlarge.postgres" = "5000"
    "db.r3.8xlarge.aurora"   = "5000"

    "db.r4.large"            = "1211"
    "db.r4.large.aurora"     = "1000"
    "db.r4.large.postgres"   = "1600"
    "db.r4.xlarge"           = "2423"
    "db.r4.xlarge.aurora"    = "2000"
    "db.r4.xlarge.postgres"  = "3200"
    "db.r4.2xlarge"          = "4847"
    "db.r4.2xlarge.aurora"   = "3000"
    "db.r4.2xlarge.postgres" = "5000"
    "db.r4.4xlarge"          = "9695"
    "db.r4.4xlarge.aurora"   = "4000"
    "db.r4.4xlarge.postgres" = "5000"
    "db.r4.8xlarge.aurora"   = "5000"
    "db.r4.16xlarge.aurora"  = "6000"
  }
}

variable "cw_rds_cpu_threshold" {
  description = "Maximum CPU utilization to trigger alert"
  type        = string
  default     = "60"
}

variable "cw_rds_cpu_periods" {
  description = "Check interval - Maximum CPU utilization to trigger alert"
  type        = string
  default     = "300"
}

variable "cw_rds_cpu_occurance" {
  description = "Number of occurances for checks - Maximum CPU utilization to trigger alert"
  type        = string
  default     = "3"
}

variable "cw_rds_disk_threshold" {
  description = "Low disk % threshold"
  type        = string
  default     = "10"
}

variable "cw_rds_disk_queue_depth_threshold" {
  description = "Disk queue depth threshold"
  type        = string
  default     = ""
}

variable "cw_rds_read_latency_threshold" {
  description = "Read latency threshold in seconds"
  type        = string
  default     = ""
}

variable "cw_rds_write_latency_threshold" {
  description = "Write latency threshold in seconds"
  type        = string
  default     = ""
}

variable "cw_rds_swap_threshold" {
  description = "Swap usage threshold in bytes"
  type        = string
  default     = ""
}

variable "cw_rds_replica_lag" {
  description = "Replica Lag threshold in seconds"
  type        = string
  default     = "2"
}

variable "enabled_cloudwatch_logs_exports" {
  description = "Log types to enable for exporting to CloudWatch logs"
  type        = list(string)
  default     = []
}

/* RDS Variables */
variable "engine" {
  description = "The database engine to use for this RDS instance"
  type        = string
  default     = "mysql"
}

variable "engine_version" {
  description = "The version for the engine specified in the 'engine' variable"
  type        = string
  default     = "5.6.27"
}

variable "character_set_name" {
  description = "The character set name to use for DB encoding in Oracle instances"
  type        = string
  default     = ""
}

variable "parameter_group" {
  description = "Name of the DB parameter group to associate"
  type        = string
  default     = "default.mysql5.6"
}

variable "option_group" {
  description = "Name of the DB option group to associate"
  type        = string
  default     = ""
}

variable "snapshot_identifier" {
  description = "Specifies whether or not to create this database from a snapshot. This correlates to the snapshot ID you would find in the RDS console"
  type        = string
  default     = ""
}

variable "read_replica_skip_final_snapshot" {
  description = "Determines whether a final DB snapshot is created before the read replica DB instance is deleted"
  type        = string
  default     = true
}

variable "security_groups" {
  description = "VPC Security Group IDs"
  type        = list(string)
  default     = []
}

variable "backup_retention_period" {
  description = "The count in days to retain backups for"
  type        = string
  default     = "0"
}

variable "backup_window" {
  description = " The daily time range (UTC) during which automated backups are created if they are enabled e.g. '09:46-10:16'. Must not overlap with maintenance_window"
  type        = string
  default     = "02:00-03:30"
}

variable "maintenance_window" {
  description = "The window to perform maintenance in. Syntax: 'ddd:hh24:mi-ddd:hh24:mi' e.g. 'Mon:00:00-Mon:03:00'"
  type        = string
  default     = "sun:03:35-sun:05:45"
}

variable "auto_minor_version_upgrade" {
  description = "Determines whether minor version upgrades are allowed by the RDS instance(s)"
  type        = string
  default     = "true"
}

variable "apply_immediately" {
  description = "Specifies whether any database modifications are applied immediately, or during the next maintenance window"
  type        = string
  default     = "false"
}

variable "license_model" {
  description = "License model information for this DB instance (required for some DB engines, i.e. Oracle SE1)"
  type        = string
  default     = ""
}

variable "skip_final_snapshot" {
  description = "Determines whether a final DB snapshot is created before the master DB instance is deleted"
  type        = string
  default     = "false"
}

variable "final_snapshot_identifier" {
  description = "The name of your final DB snapshot when this DB instance is deleted"
  type        = string
  default     = ""
}

variable "allow_major_version_upgrade" {
  description = "Determines whether major version upgrades are allowed by the RDS instance(s)"
  type        = string
  default     = "false"
}

variable "enabled" {
  description = "Whether or not to enable or disable the RDS instance. This allows the flexibility to enable or disable resources per environment"
  type        = bool
  default     = true
}

variable "db_subnet_group_name" {
  description = "Specifies the DB subnet group name"
  type        = string
  default     = ""
}

variable "ca_cert_identifier" {
  description = "Specifies the identifier of the CA certificate for the DB instance."
  type        = string
  default     = "rds-ca-2019"
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "kms_key_id" {
  description = "Specifies the KMS key to use for encryption"
  type        = string
  default     = null
}

variable "performance_insights_enabled" {
  description = "Enables performance insights"
  type        = bool
  default     = false
}

variable "performance_insights_kms_key_id" {
  description = "The ARN for the KMS key to encrypt Performance Insights data. When specifying performance_insights_kms_key_id, performance_insights_enabled needs to be set to true. Once KMS key is set, it can never be changed."
  type        = string
  default     = ""
}
