data "aws_iam_policy_document" "monitoring_assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["monitoring.rds.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "monitoring_role" {
  count              = var.enabled == true && var.monitoring_interval > 0 ? 1 : 0
  name               = "${var.name}-monitoring-role"
  assume_role_policy = data.aws_iam_policy_document.monitoring_assume_role.json
  tags               = var.tags
}

data "aws_iam_policy_document" "monitoring_policy" {
  statement {
    sid    = "EnableCreationAndManagementOfRDSCloudwatchLogGroups"
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:PutRetentionPolicy",
    ]

    resources = [
      "arn:aws:logs:*:*:log-group:RDS*",
    ]
  }

  statement {
    sid    = "EnableCreationAndManagementOfRDSCloudwatchLogStreams"
    effect = "Allow"

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams",
      "logs:GetLogEvents",
    ]

    resources = [
      "arn:aws:logs:*:*:log-group:RDS*:log-stream:*",
    ]
  }
}

resource "aws_iam_role_policy" "monitoring_policy" {
  count  = var.enabled == true && var.monitoring_interval > 0 ? 1 : 0
  name   = "rds-enhanced-monitoring"
  role   = join("", aws_iam_role.monitoring_role.*.id)
  policy = data.aws_iam_policy_document.monitoring_policy.json
}
