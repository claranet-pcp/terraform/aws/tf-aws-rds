output "endpoint" {
  value = join("", aws_db_instance.default.*.endpoint)
}

output "address" {
  value = join("", aws_db_instance.default.*.address)
}

output "arn" {
  value = join("", aws_db_instance.default.*.arn)
}

output "identifier" {
  value = join("", aws_db_instance.default.*.identifier)
}

output "read_replica_addresses" {
  value = aws_db_instance.read_replica.*.address
}
